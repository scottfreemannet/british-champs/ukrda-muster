const set = (name, value, days) => {
    let expires = "";
    if (!name) { return new Error("Cookie name not specified"); }
    if (!value) { return new Error("Cookie value not specified"); }
    if (days) {
        if (typeof days !== "number") { return new Error("Cookie expiration must be a number of days"); }
        let date = new Date();
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
};

const get = (name) => {
    let nameEQ = name + "=",
        ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
};

const del = (name) => {
    let date = new Date();
    date.setTime(date.getTime() - 1 * 24 * 60 * 60 * 1000);
    document.cookie = name + "=; expires=" + date.toGMTString() + "; path=/";
};

export { set, get, del };