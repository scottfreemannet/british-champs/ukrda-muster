const create = (json, container, options) => {

    let col = [],
        strip = options.remove ? options.remove : [],
        append = options.append ? options.append : [];

    for (let i = 0; i < json.length; i++) {
        for (let key in json[i]) {
            if (col.indexOf(key) === -1) {
                if (!strip.includes(key)) {
                    col.push(key);
                }
            }
        }
    }

    let table = document.createElement("table");
    let tr = table.insertRow(-1);

    for (let i = 0; i < col.length; i++) {
        let th = document.createElement("th");
        th.innerHTML = col[i];
        tr.appendChild(th);
    }
    for (let o = 0; o < append.length; o++) {
        let th = document.createElement("th");
        tr.appendChild(th);
    }

    for (let i = 0; i < json.length; i++) {
        tr = table.insertRow(-1);
        for (let j = 0; j < col.length; j++) {
            let tabCell = tr.insertCell(-1);
            tabCell.innerHTML = json[i][col[j]];
        }
        for (let j = 0; j < append.length; j++) {
            let tabCell = tr.insertCell(-1);
            let text = append[j].replace("${id}", json[i]['id']);
            tabCell.innerHTML = text;
        }
    }

    let divContainer = document.getElementById(container);
    divContainer.innerHTML = "";
    divContainer.appendChild(table);
};

export { create };