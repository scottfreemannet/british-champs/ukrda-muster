import * as jsonToTable from  './utils/jsonToTable';
import * as cookie from  './utils/cookies';

class Admin {

    check() {
        return new Promise(function(resolve, reject) {
            window.M.Api.fetch({
                request: 'admin-check'
            })
            .then(response => response.ok == "1" ? resolve() : window.location.href = "/");
        });
    }
    
    users() {
        this.check().then(() => {
            window.M.Api.fetch({
                request: 'admin-users'
            })
            .then(response => {
                jsonToTable.create(response, "userList", {
                    remove: ['id', 'active'],
                    append: [
                        //'<a href="/?admin/user-toggle#${id}" class="btn">Disable</a>',
                        '<a href="/?admin/user-login#${id}" class="btn">Log in as user</a>'
                    ]
                });
            });
        });
    }

    userLogin() {
        window.M.Api.fetch({
            request: 'admin-login-as',
            user: window.location.hash.replace("#","")
        })
        .then(response => {
            window.M.user = response.user;
            cookie.set('_u', JSON.stringify(response.user), 30);
            window.location.href = "/";
        });
    }

    leagues() {
        this.check().then(() => {
            window.M.Api.fetch({
                request: 'admin-leagues'
            })
            .then(response => {
                jsonToTable.create(response, "leagueList", {
                    remove: ['id', 'active'],
                    append: [
                        '<a href="/?admin/league#${id}" class="btn">View League</a>'
                    ]
                });
            });
        });
    }

    league() {
        this.check().then(() => {
            let id = window.location.hash.replace("#","");
            window.M.Api.fetch({
                request: 'admin-league',
                id: id
            })
            .then(response => {
                document.getElementById('leagueName').innerText = response.name;
            });

            window.M.Api.fetch({
                request: 'admin-league-charters',
                id: id
            })
            .then(response => {
                jsonToTable.create(response, "charterTable", {
                    remove: ['id', 'active'],
                    append: [
                        '<a href="/?admin/charter#' + id + '-${id}" class="btn">View Charter</a>'
                    ]
                });
                let ele = document.createElement('a');
                let txt = document.createTextNode("Back");
                ele.setAttribute('class', 'btn');
                ele.setAttribute('href', `/?admin/leagues`);
                ele.appendChild(txt);
                document.getElementById('charterTable').appendChild(ele);
            })
            .then(() => {
                let activeCharter = [];
                let table = document.querySelector("#charterTable table");
                for (let row of table.rows)  {
                    let submitted = row.cells[1];
                    let submittedText = submitted.innerText;
                    let daysAgo = this.daysAgo(submittedText);
                    if (submittedText !== "" && submittedText !== "Submitted") {
                        submitted.innerText = `${submittedText} (${daysAgo})`;
                        let days = daysAgo.split(" ")[0];
                        if (days > 29) {
                            let leagueName = row.cells[1].innerText.replace(/[\W_]+/g,"");
                            row.setAttribute('data-daysOld', days);
                            row.setAttribute('data-league', leagueName);
                            let existing = activeCharter[leagueName] ? activeCharter[leagueName] : 99999999;
                            activeCharter[leagueName] = Math.min(days,existing);
                        }
                    }
                }
                let lowest = 99999999;
                let league;
                
                for (let [k,v] of Object.entries(activeCharter)) {
                    if (v < lowest) {
                        lowest = v;
                        league = k;
                    }
                }

                let activeRow = document.querySelector(`#charterTable table tr[data-daysOld="${lowest}"][data-league="${league}"]`);
                    activeRow.className = "charter-active";
            });
        });
    }

    charter() {
        this.check().then(() => {
            let hash = window.location.hash.replace("#","");
            let leagueId = hash.split("-")[0];
            let charterId = hash.split("-")[1];
            window.M.Api.fetch({
                request: 'admin-league',
                id: leagueId
            })
            .then(response => {
                document.getElementById('leagueName').innerText = response.name;
            });
            window.M.Api.fetch({
                request: 'admin-charter',
                id: charterId
            })
            .then(response => {
                document.getElementById('charterCreated').innerText = response.created;
                document.getElementById('charterSubmitted').innerText = response.submitted;
            });
            window.M.Api.fetch({
                request: 'admin-league-charter',
                id: charterId
            })
            .then(response => {
                jsonToTable.create(response, "skaterTable", {
                    remove: ['id', 'active'],
                    append: [
                        '<a href="#" data-skaterid="${id}" class="btn js-edit-skater">Edit</a>'
                    ]
                });
                let ele = document.createElement('a');
                let txt = document.createTextNode("Back");
                ele.setAttribute('class', 'btn');
                ele.setAttribute('href', `/?admin/league#${leagueId}`);
                ele.appendChild(txt);
                document.getElementById('skaterTable').appendChild(ele);
            })
            .then(() => {
                document.querySelectorAll('.js-edit-skater').forEach((e) => {
                    e.addEventListener('click', (e) => {
                        e.preventDefault();
                        let skaterRow = e.target.parentElement.parentElement;
                        let skaterId = e.target.getAttribute('data-skaterid');
                        if (e.target.innerText == "Edit") {
                            this.editSkaterRow(skaterRow);
                        } else {
                            this.saveSkaterRow(skaterRow, skaterId);
                        }
                    });
                });
            })
            .then(() => {
                document.querySelector('input[name="adminNewSubmitted"]').valueAsDate = new Date(document.getElementById('charterSubmitted').innerText);
                document.getElementById("newSubmittedForm").addEventListener("submit", (e) => {
                    e.preventDefault();
                    window.M.Api.fetch({
                        request: "admin-change-submitted",
                        charter: charterId,
                        date: document.querySelector('input[name="adminNewSubmitted"]').value
                    })
                    .then(() => {
                        window.location.reload();
                    });
                });
            });
        });
    }

    editSkaterRow(skaterRow) {
        let skaterNumber = skaterRow.firstChild.innerText;
        let skaterName = skaterRow.firstChild.nextElementSibling.innerText;
        skaterRow.firstChild.innerHTML = `<input type="number" value="${skaterNumber}" class="single-skater--number">`;
        skaterRow.firstChild.nextElementSibling.innerHTML = `<input type="text" value="${skaterName}" class="single-skater--name">`;
        skaterRow.firstChild.nextElementSibling.innerHTML = `<input type="text" value="${skaterName}" class="single-skater--name">`;
        skaterRow.lastElementChild.firstElementChild.innerText = "Save";
    }

    saveSkaterRow(skaterRow, skaterId) {
        let skaterNumber = skaterRow.firstElementChild.firstElementChild.value;
        let skaterName = skaterRow.firstChild.nextElementSibling.firstElementChild.value;
        window.M.Api.fetch({
            request: 'admin-skater-update',
            id: skaterId,
            update: {
                name: skaterName,
                number: skaterNumber
            }
        })
        .then(response => {
            if (response === true) {
                window.location.reload();
            } else {
                alert("Unable to update skater record");
            }
        });
    }
    
    daysAgo(date) {
        let now = new Date();
        let then = new Date(date);
        let timeDiff = Math.abs(now.getTime() - then.getTime());
        let dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
        let pl = dayDiff === 1 ? "" : "s";
        return `${dayDiff} day${pl} ago`;
    }

}

export default Admin;