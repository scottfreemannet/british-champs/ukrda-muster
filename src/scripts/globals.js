class Globals {

    success(form, msg) {
        let el = document.createElement('div');
        let message = document.createTextNode(msg);
        el.setAttribute('class', 'alert alert-success');
        el.setAttribute('id', 'error');
        el.appendChild(message);
        document.getElementById(form).appendChild(el);
    }

    error(form, msg) {
        let el = document.createElement('div');
        let message = document.createTextNode(msg);
        el.setAttribute('class', 'alert alert-error');
        el.setAttribute('id', 'error');
        el.appendChild(message);
        document.getElementById(form).appendChild(el);
    }
    
    clearError() {
        var elem = document.getElementById('error');
        if (elem) {
            elem.parentNode.removeChild(elem);
        }
    }
}

export default Globals;