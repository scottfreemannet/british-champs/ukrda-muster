/* eslint-disable indent */
import * as cookie from  './utils/cookies';

class Login {
    init() {
        if (document.getElementById("login")) {
            document.getElementById("login").addEventListener("submit", this, false);
            document.getElementById("register").addEventListener("submit", this, false);
        }
        if (document.getElementById("forgot")) {
            document.getElementById("forgot").addEventListener("submit", this, false);
        }
        if (document.getElementById("reset")) {
            document.getElementById("reset").addEventListener("submit", this, false);
        }
        if (document.getElementById("activate")) {
            this.activate();
        }
        if (window.location.hash === "#r=1") {
            window.M.Globals.success("login", "Password changed. Please log in with your new credentials.");
        }
        if (window.location.hash === "#a=1") {
            window.M.Globals.success("login", "Account activated. Please log in with your credentials.");
        }
    }

    handleEvent(evt) {
        evt.preventDefault();        
        if (evt.target.id === "login") {
            window.M.Globals.clearError("login");
            this.login({
                email: document.querySelector('#login input[name="email"]').value,
                password: document.querySelector('#login input[name="password"]').value
            });
        }
        if (evt.target.id === "register") {
            window.M.Globals.clearError("register");
            this.register({
                email: document.querySelector('#register input[name="email"]').value,
                password1: document.querySelector('#register input[name="password1"]').value,
                password2: document.querySelector('#register input[name="password2"]').value
            });
        }
        if (evt.target.id === "forgot") {
            window.M.Globals.clearError("forgot");
            this.forgot({
                email: document.querySelector('#forgot input[name="email"]').value
            });
        }
        if (evt.target.id === "reset") {
            window.M.Globals.clearError("reset");
            this.reset({
                password1: document.querySelector('#reset input[name="password1"]').value,
                password2: document.querySelector('#reset input[name="password2"]').value
            });
        }
    }

    login(user) {
        window.M.Api.fetch({
            request: 'login',
            email: user.email,
            password: user.password
        })
        .then(response => {
            if (response.error) {
                window.M.Globals.error('login',response.error);
            } else {
                window.M.user = response;
                cookie.set('_u', JSON.stringify(response), 30);
                window.location.href = "/";
            }
        });
    }

    checkCredentials() {
        return new Promise(function(resolve, reject) {
            let cookieData = JSON.parse(cookie.get('_u'));
            if (cookieData) {                
                return new Promise(function() {
                    window.M.Api.fetch({
                        request: 'validate',
                        email: cookieData.email,
                        token: cookieData.token
                    })
                    .then(response => {
                        if (response.valid === "true") {
                            window.M.user = cookieData;
                            resolve(response);
                        } else {
                            reject();
                        }
                    });                
                });
            } else {
                reject();
            }
        });
    }

    register(user) {
        this.checkRegisterForm(user)
        .then(user => {
            window.M.Api.fetch({
                request: 'register',
                insert: {
                    email: user.email,
                    password: user.password1
                }
            })
            .then(response => {
                if (response.error) {
                    window.M.Globals.error('register',response.error);
                } else {
                    window.M.Globals.success('register',"Please check your email for an activation link");
                }
            });
        })
        .catch(error => {
            window.M.Globals.error("register", error);
        });
    }

    checkRegisterForm(user) {
        return new Promise(function(resolve, reject) {
            if (user.password1 !== user.password2) {
                reject("Passwords do not match");
            }

            if (user.password1.length < 7) {
                reject("Passwords needs to be at least eight characters");
            }
            if (!/\d/.test(user.password1)) {
                reject("Passwords need to contain at least one number");
            }
            if (!/[a-z]/i.test(user.password1)) {
                reject("Passwords need to contain at least one letter");
            }
            if (user.password1.toUpperCase() == user.password1 || user.password1.toLowerCase() == user.password1) {
                reject("Passwords need to contain at least one uppercase and one lowercase character");
            }
            if (user.email) {
                return new Promise(function() {
                    window.M.Api.fetch({
                        request: 'usernameavailable',
                        email: user.email
                    })
                    .then(response => {
                        response.available === "true" ? resolve(user) : reject("Username is already in use.");
                    });
                });
            } else {
                resolve(user);
            }
        });
    }

    forgot(user) {
        window.M.Api.fetch({
            request: 'forgot',
            email: user.email
        })
        .then(response => {
            if (response.error) {
                window.M.Globals.error('forgot',response.error);
            } else {
                window.M.Globals.success('forgot',response.message);
            }
        });
    }

    reset(user) {
        this.checkRegisterForm(user)
        .then(user => {
            let emailkey = window.location.hash.replace("#",'');
            window.M.Api.fetch({
                request: 'reset',
                emailkey: emailkey,
                insert: {
                    password: user.password1
                }
            })
            .then(response => {
                if (response.error) {
                    window.M.Globals.error('reset',response.error);
                } else {
                    window.location.href = "/?#r=1";
                }
            });
        })
        .catch(error => {
            window.M.Globals.error("reset", error);
        });
    }

    activate() {
        let emailkey = window.location.hash.replace("#",'');
        window.M.Api.fetch({
            request: 'activate',
            emailkey: emailkey
        })
        .then(response => {
            if (response.error) {
                window.M.Globals.error('reset',response.error);
            } else {
                window.location.href = "/?#a=1";
            }
        });
    }

    logout() {
        cookie.del("_u");
        window.location.href = "/";
    }
}

export default Login;