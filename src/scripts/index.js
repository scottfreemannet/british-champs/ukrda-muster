import cssUrl from '../styles/muster.scss';
import Muster from './muster';
import CI_ENDPOINT from './url';

const PRODUCTION_ENDPOINT = "https://muster.britishchamps.com/api";
const DEVELOPMENT_ENDPOINT = "http://localhost:8000";

const defaultOptions = {
    API_ENDPOINT: CI_ENDPOINT.length ? CI_ENDPOINT :
        (process.env.NODE_ENV === "production" ? PRODUCTION_ENDPOINT : DEVELOPMENT_ENDPOINT)
};

window.M = new Muster(defaultOptions);
window.M.init();
