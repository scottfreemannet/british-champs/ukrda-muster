import * as jsonToTable from  './utils/jsonToTable';

class Charters {
    init() {
        if (document.getElementById("new")) {
            document.getElementById("new").addEventListener("submit", this, false);
        };

        if (document.getElementById("edit")) {
            this.disabled = false;
            window.M.Api.fetch({
                request: "charter-fetch-single",
                id: window.location.hash.replace("#","")
            })
            .then(response => {
                if (response.length === 0) {
                    window.location="/?charters";
                } else {
                    response[0].submitted != "0000-00-00" ? this.disableEditing() : "";
                    document.getElementById('edit').classList = "";
                    this.selectLeague(response[0].league_id);
                    this.fetchCharterSkaters(response[0].id);
                    this.checkSubmitted(response[0].submitted);
                }
            });

            document.getElementById("edit").addEventListener("submit", this, false);
            document.getElementById("addSkater").addEventListener("click", this, false);
            document.getElementById("charterSubmit").addEventListener("click", this, false);            
        }

        if (document.getElementById('leagueSelect')) {
            window.M.Api.fetch({
                request: "league-fetch"
            })
            .then(response => {
                let select = document.getElementById('leagueSelect');
                for (let resp of response) {
                    var option = document.createElement("option");
                    option.text = resp.name;
                    option.value = resp.id;
                    select.add(option);
                }
            })
            .then(() => {
                if (window.location.hash.length > 0) {
                    let val = window.location.hash.replace("#","");
                    if (document.querySelector('#leagueSelect [value="' + val + '"]')) {
                        document.querySelector('#leagueSelect [value="' + val + '"]').selected = true;
                    }
                }
            });
            window.M.Api.fetch({
                request: "charter-fetch-skaters",
                id: document.location.hash.replace("#",'')
            })
            .then(response => {
                if (response.length > 0) {
                    document.querySelector('#charterDuplicate').classList = "";
                    jsonToTable.create(response, "charterDuplicate", {
                        remove: ['id']
                    });
                }
            });
        }
    }



    handleEvent(evt) {
        evt.preventDefault();
        window.M.Globals.clearError("new");
        if (evt.target.id === "new") {
            this.create()
            .then((id) => window.location.href="/?charters/edit#" + id)
            .catch((e) => window.M.Globals.error("new", e));
        }
        if (evt.target.id === "edit") {
            this.edit()
            .then(() => window.location.reload())
            .catch((e) => {
                this.charterError(e);
            });
        }
        if (evt.target.id === "addSkater") {
            this.addSkaterRow();
        }
        if (evt.target.id === "charterSubmit") {
            this.submitCharter();
        }
    }    

    fetch() {
        window.M.Api.fetch({
            request: "charter-fetch",
            name: name
        })
        .then(response => {
            jsonToTable.create(response, "charterTable", {
                remove: ['id'],
                append: [
                    '<a href="/?charters/edit#${id}" class="btn">Edit</a>',
                    '<a href="/?charters/new#${id}" class="btn">Duplicate</a>',
                    '<a href="/?charters/delete#${id}" class="btn negative">Delete</a>'
                ]
            });
        })
        .then(() => {
            let activeCharter = [];
            let table = document.querySelector("#charterTable table");
            for (let row of table.rows)  {
                let submitted = row.cells[2];
                let submittedText = submitted.innerText;
                let daysAgo = this.daysAgo(submittedText);
                if (submittedText !== "" && submittedText !== "Submitted") {
                    row.cells[3].querySelector('a').innerText = "View";
                    row.cells[5].innerText = "";
                    submitted.innerText = `${submittedText} (${daysAgo})`;
                    let days = daysAgo.split(" ")[0];
                    if (days > 29) {
                        let leagueName = row.cells[1].innerText.replace(/[\W_]+/g,"");
                        row.setAttribute('data-daysOld', days);
                        row.setAttribute('data-league', leagueName);
                        let existing = activeCharter[leagueName] ? activeCharter[leagueName] : 99999999;
                        activeCharter[leagueName] = Math.min(days,existing);
                    }
                }
            }
            for (let [k,v] of Object.entries(activeCharter)) {
                let activeRow = document.querySelector(`#charterTable table tr[data-daysOld="${v}"][data-league="${k}"]`);
                activeRow.className = "charter-active";
            }
            
        });
    }

    create() {
        return new Promise(function(resolve, reject) {
            window.M.Api.fetch({
                request: "charter-create",
                league: document.querySelector('#new select[name="league"]').value,
                duplicate: window.location.hash.replace("#","")
            })
            .then(response => response.success === "true" ? resolve(response.id) : reject(response.message));
        });
    }

    delete() {
        let id = window.location.hash.replace("#","");
        window.M.Api.fetch({
            request: "charter-delete",
            id: id
        })
        .then(() => window.location.href=`/?charters`)
        .catch((e) => window.location.href=`/?charters`);
    }

    edit() {
        return new Promise(function(resolve, reject) {
            window.M.Api.fetch({
                request: "charter-fetch-single",
                id: window.location.hash.replace("#","")
            })
            .then(response => {
                if (response.length === 1) {
                    let data = [];
                    let skaterData = document.querySelectorAll('.js-skater-template');
                    for (let skater of skaterData) {
                        let number = skater.querySelector('input[data-name="number"]').value;
                        let name = skater.querySelector('input[data-name="name"]').value;    
                        if (!(number.length === 0 && name.length === 0)) {
                            if (isNaN(number)) {
                                reject({error: "Invalid skater number"});
                            }
                            if (number.length < 1 || number.length > 4) {
                                reject({error: "Invalid skater number"});
                            }
                            if (name.length < 1 || number.length > 254) {
                                reject({error: "Invalid skater name"});
                            }
                            data.push({
                                number: number,
                                name: name
                            });
                        }
                    }
                    if (data.length > 0) {
                        window.M.Api.fetch({
                            request: "charter-edit",
                            id: response[0].id,
                            insert: data
                        })
                        .then(response => response.success === "true" ? resolve(response) : reject(response));
                    } else {
                        reject({error: "Nothing to insert"});
                    }
                } else {
                    reject({error: "Unable to update charter"});
                }
            });
        });
    }

    addSkaterRow() {
        let numberOfSkaters = document.querySelectorAll('#skaterTable tr').length - 1;
        numberOfSkaters += document.querySelectorAll('.js-skater-template:not(.hidden)').length;
        if (numberOfSkaters === 20) {
            window.M.Globals.error("skaterTemplate", "Maximum number of skaters reached.");
            return false;
        }

        let template = document.querySelector("#skaterTemplate").childNodes[1].cloneNode(true);
        let skaterNumber = document.querySelectorAll('.js-skater-template').length + 1;
        template.className = "js-skater-template skater-form";
        for (let el of template.querySelectorAll('input[name="number1"]')) {
            el.setAttribute("name", "number" + skaterNumber);
            el.value = "";
        }
        for (let el of template.querySelectorAll('input[name="name1"]')) {
            el.setAttribute("name", "name" + skaterNumber);
            el.value = "";
        }    
        document.getElementById("skaterTemplate").appendChild(template, document.getElementById("skaterTemplate").nextSibling);
    }

    selectLeague(id) {
        return window.M.Api.fetch({
            request: "league-fetch-single",
            id: id
        })
        .then(resp => {
            document.getElementById('leagueName').value = resp[0].name;
        });
    }

    fetchCharterSkaters(id) {
        window.M.Api.fetch({
            request: "charter-fetch-skaters",
            id: id
        })
        .then(resp => {
            let append = this.disabled ? [] : ['<a href="/?skaters/delete#${id}" class="btn deleteSkater negative">Delete</a>'];
            jsonToTable.create(resp, "skaterTable", {
                remove: ['id'],
                append: append
            });
        });
    }

    charterError(error) {
        window.M.Globals.error("edit", error.error);
        // for (let row in error.data) {
        //     console.log(error.data[row].number, error.data[row].name);
        // }
    }

    disableEditing() {
        this.disabled = true;
        document.querySelector('h1').innerText = "View Charter";
        document.getElementById('skaterTemplate').remove();
        document.getElementById('addSkater').remove();        
        for (let el of document.querySelectorAll('.deleteSkater')) {
            el.remove();
        }
        for (let el of document.querySelectorAll('button')) {
            if (!el.hasAttribute('data-admin')) {
                el.remove();
            }
        }
        for (let el of document.querySelectorAll('.js-edit-mode')) {
            el.remove();
        }
    }

    checkSubmitted(submitted) {
        let output = "";
        if (submitted !== "0000-00-00") {
            output += `${submitted} (${this.daysAgo(submitted)})`;
        }
        document.getElementById('submitted').value = output;
    }

    daysAgo(date) {
        let now = new Date();
        let then = new Date(date);
        let timeDiff = Math.abs(now.getTime() - then.getTime());
        let dayDiff = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
        let pl = dayDiff === 1 ? "" : "s";
        return `${dayDiff} day${pl} ago`;
    }

    submitCharter() {
        window.M.Api.fetch({
            request: "charter-submit",
            id: window.location.hash.replace("#","")
        })
        .then(response => {
            if (response) {
                window.location.reload();
            }
        });
    }
    
}

export default Charters;