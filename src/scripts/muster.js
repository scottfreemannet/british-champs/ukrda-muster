import Globals from './globals';
import Api from './api';
import Login from './login';
import Page from './page';
import Leagues from './leagues';
import Charters from './charters';
import Skaters from './skaters';
import Admin from './admin';

class Muster {    
    constructor(options) {
        this.API_ENDPOINT = options.API_ENDPOINT;
        this.Globals = new Globals(options);
        this.Api = new Api(options);
        this.Login = new Login(options);
        this.Page = new Page(options);
        this.Leagues = new Leagues(options);
        this.Charters = new Charters(options);
        this.Skaters = new Skaters(options);
        this.Admin = new Admin(options);
    }

    init() {
        this.Login.checkCredentials()
        .then((response) => {
            let page = "dashboard";
            if (window.location.search) {
                var urlParams = new URLSearchParams(window.location.search);
                for(var key of urlParams.keys()) { 
                    page = key;
                }
            }
            this.Page.fetch(page);
            this.Page.nav(response.user.navigation);
        })
        .catch(() => {
            let page = "login";
            if (window.location.search) {
                var urlParams = new URLSearchParams(window.location.search);
                for(var key of urlParams.keys()) { 
                    page = key;
                }
                page = page.includes("login") ? page : "login";
            }
            this.Page.fetch(page);
            this.Page.nav('global');
        });
    }
}

Muster.prototype.version = '0.0.1';

export default Muster;