class Page {
    fetch(page) {
        fetch(`/pages/${page}.html`)
        .then(response => response.text())
        .then(response => {
            let parser = new DOMParser();
            let doc = parser.parseFromString(response, "text/html");
            let script = doc.getElementById('musterScript');
            document.getElementById('content').innerHTML = response;
            if (script) {
                this.musterScript(script.innerHTML);
            }
        });
    }

    nav(page) {
        fetch(`/pages/nav_${page}.html`)
        .then(response => response.text())
        .then(response => {
            document.getElementById('nav').innerHTML = response;
            if (document.getElementById('metaUsername')) {
                document.getElementById('metaUsername').innerText = window.M.user.email;
            }
        });
    }

    musterScript(script) {
        return Function('"use strict";return (' + script + ')')();
    }
    
}

export default Page;