class Api {
    fetch(data) {
        if (window.M.user) {
            data.token = window.M.user.token;
        }
        return fetch(`${window.M.API_ENDPOINT}/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => response.json());
    }
}

export default Api;