class Skaters {

    delete() {
        let id = window.location.hash.replace("#","");
        window.M.Api.fetch({
            request: "skater-delete",
            id: id
        })
        .then((response) => {
            window.location.href=`/?charters/edit#${response[0].charter_id}`;
        })
        .catch((e) => window.location.href=`/?charters`);
    }

}

export default Skaters;