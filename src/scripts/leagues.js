import * as jsonToTable from  './utils/jsonToTable';

class Leagues {
    init() {
        document.getElementById("new") ? document.getElementById("new").addEventListener("submit", this, false) : "";
        if (document.getElementById("edit")) {
            window.M.Api.fetch({
                request: "league-fetch-single",
                id: window.location.hash.replace("#","")
            })
            .then(response => {
                response.length === 0 ? window.location="/?leagues" : document.querySelector('#edit input[name="name"]').value = response[0].name;
            });
            document.getElementById("edit").addEventListener("submit", this, false);            
        }
    }

    handleEvent(evt) {
        evt.preventDefault();
        window.M.Globals.clearError("new");
        if (evt.target.id === "new") {
            this.create(document.querySelector('#new input[name="name"]').value)
            .then(() => window.location.href="/?leagues")
            .catch((e) => window.M.Globals.error("new", e));
        }
        if (evt.target.id === "edit") {
            this.edit()
            .then(() => window.location.href="/?leagues")
            .catch((e) => window.M.Globals.error("edit", "Unable to edit league."));
        }
    }

    fetch() {
        window.M.Api.fetch({
            request: "league-fetch",
            name: name
        })
        .then(response => {
            jsonToTable.create(response, "leagueTable", {
                remove: ['id'],
                append: [
                    '<a href="/?leagues/edit#${id}" class="btn">Edit</a>',
                    '<a href="/?charters/new#${id}" class="btn">New Charter</a>',
                    '<a href="/?leagues/delete#${id}" class="btn negative">Delete</a>'
                ]
            });
        });
    }

    create(name) {
        return new Promise(function(resolve, reject) {
            window.M.Api.fetch({
                request: "league-create",
                insert: {
                    name: name
                }
            })
            .then(response => response.success === "true" ? resolve() : reject(response.message));
        });
    }

    delete() {
        let id = window.location.hash.replace("#","");
        window.M.Api.fetch({
            request: "league-delete",
            id: id
        })
        .then(() => window.location.href="/?leagues")
        .catch((e) => window.location.href="/?leagues");
    }

    edit() {
        return new Promise(function(resolve, reject) {
            window.M.Api.fetch({
                request: "league-fetch-single",
                id: window.location.hash.replace("#","")
            })
            .then(response => {
                if (response.length === 1) {
                    window.M.Api.fetch({
                        request: "league-edit",
                        id: response[0].id,
                        insert: {
                            name: document.querySelector('#edit input[name="name"]').value
                        }
                    })
                    .then(response => response.success === "true" ? resolve() : reject());
                } else {
                    reject();
                }
            });
        });
    }
    
}

export default Leagues;