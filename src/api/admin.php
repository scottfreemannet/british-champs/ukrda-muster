<?php

if (!empty($data)) {

    if ($data['request'] == "admin-change-submitted") {
        $db->where("id", $data['charter']);
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $insert['submitted'] = $data['date'];
        $charters = $db->update("charters", $insert);
        output($charters);
    }

    if ($data['request'] == "admin-check") {
        output(array("ok" => $_SESSION['admin']));
    }

    if ($_SESSION['admin']) {

        if ($data['request'] == "admin-users") {
            $db->orderBy("email", "asc");
            $db->where("admin","0");
            $users = $db->get("users", null, "id, email, active");
            output($users);
        }

        if ($data['request'] == "admin-login-as") {
            $db->where("id", $data['user']);
            $db->where("active", "1");
            $users = $db->get("users");
            $_SESSION['id'] = $users[0]['id'];
            $_SESSION['email'] = $users[0]['email'];
            $_SESSION['password'] = $users[0]['password'];
            unset($_SESSION['admin']);

            $user = $users[0];
            $user['id'] = $users[0]['id'];
            $user['email'] = $users[0]['email'];
            $user['token'] = session_id();

            output(array(
                "success" => "true",
                "user" => $user,
            ));        
        }

        if ($data['request'] == "admin-leagues") {
            $db->orderBy("name", "asc");
            $leagues = $db->get("leagues", null, "id, name");
            output($leagues);
        }

        if ($data['request'] == "admin-league") {
            $db->where("id", $data['id']);
            $league = $db->getOne("leagues", null, "name");
            output($league);
       }

       if ($data['request'] == "admin-charter") {
            $db->where("id", $data['id']);
            $charter = $db->getOne("charters", null, "created, IF(submitted='0000-00-00','',submitted) as submitted");
            output($charter);
        }

        if ($data['request'] == "admin-league-charters") {
            $db->where("league_id", $data['id']);
            $db->orderBy("id", "asc");
            $users = $db->get("charters", null, "id, created, IF(submitted='0000-00-00','',submitted) as submitted");
            output($users);
        }

        if ($data['request'] == "admin-league-charter") {
            $db->where("charter_id", $data['id']);
            $db->where("active", "1");
            $db->orderBy("number", "asc");
            $users = $db->get("skaters", null, "id, number, name");
            output($users);
        }

        if ($data['request'] == "admin-skater-update") {
            $db->where("id", $data['id']);
            $insert['number'] = $data['update']['number'];
            $insert['name'] = $data['update']['name'];
            $skater = $db->update("skaters", $insert);
            output($skater);
        }
        
    }

}
?>