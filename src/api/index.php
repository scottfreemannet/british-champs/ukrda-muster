<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once('settings.php');
require_once('MysqliDb.php');

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

$data = json_decode(file_get_contents('php://input'), true);

function output($data) {
    echo json_encode($data);
    exit;
}

function sendMail($to, $subject, $template, $data = false) {
    global $EMAIL_FROM;
    global $SMTP_SERVER;
    global $SMTP_USERNAME;
    global $SMTP_PASSWORD;
    global $SMTP_USE_TLS;
    global $SMTP_PORT;

    $mail = new PHPMailer();

    try {

    // Settings
    $mail->IsSMTP();
    $mail->CharSet = 'UTF-8';

    $mail->Host       = $SMTP_SERVER;
    $mail->SMTPAuth   = $SMTP_USERNAME != "";
    $mail->Username   = $SMTP_USERNAME;
    $mail->Password   = $SMTP_PASSWORD;
    $mail->SMPTSecure = $SMTP_USE_TLS ? "tls" : "";
    $mail->Port       = $SMTP_PORT;

    $mail->setFrom($EMAIL_FROM);
    $mail->addAddress($to);
    $mail->isHTML(true);
    $mail->Subject = $subject;

    $email = file_get_contents("./emails/".$template.".html");
    if ($data) {
        foreach ($data as $key => $value) {
            $email = str_replace($key, $value, $email);
        }
    }

    $mail->Body = $email;

    $mail->send();

    echo "Sent an e-mail!";

    } catch (Exception $e) {
        echo "Failed to send an e-mail: {$mail->ErrorInfo}";
    }

}

if (!empty($data)) {
    if (array_key_exists("token", $data)) {
        session_id($data['token']);
    }
    session_start();

    $db = new MysqliDb ($DATABASE_HOST, $DATABASE_USERNAME, $DATABASE_PASSWORD, $DATABASE_NAME);

    require('login.php');
    require('leagues.php');
    require('charters.php');
    require('skaters.php');
    require('admin.php');
}
?>