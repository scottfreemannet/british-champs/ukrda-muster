<?php
//General
$SITE_URL = "";             //website url

//Database
$DATABASE_HOST = "";        //mysql server
$DATABASE_USERNAME = "";    //mysql username
$DATABASE_PASSWORD = "";    //mysql password
$DATABASE_NAME = "";        //mysql database

//Email
$EMAIL_FROM = "";           //email from address
$SMTP_SERVER = "";
$SMTP_USERNAME = "";
$SMTP_PASSWORD = "";
$SMTP_PORT = 25;
$SMTP_USE_TLS = false;

?>
