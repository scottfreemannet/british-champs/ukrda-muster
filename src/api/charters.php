<?php
if (!empty($data)) {

    if ($data['request'] == "charter-create") {
        $insert['user_id'] = $_SESSION['id'];
        $insert['league_id'] = $data['league'];
        $insert['created'] = date("Y-m-d");
        $insert['active'] = 1;
        $id = $db->insert("charters", $insert);
        if ($id) {
            if ($data['duplicate'] != "") {
                $db->where("user_id", $_SESSION['id']);
                $db->where("charter_id", $data['duplicate']);
                $db->where("active", "1");
                $skaters = $db->get("skaters", null, "number, name");

                $keys = Array("user_id","charter_id","active","number","name");
                $insertSkaters = [];
                foreach($skaters as $skater) {
                    $insertSkater = [];
                    $insertSkater['user_id'] = $_SESSION['id'];
                    $insertSkater['charter_id'] = $id;
                    $insertSkater['active'] = "1";
                    $insertSkater['number'] = $skater['number'];
                    $insertSkater['name'] = $skater['name'];
                    $insertSkaters[] = $insertSkater;
                }

                $insertSkaterId = $db->insertMulti('skaters', $insertSkaters, $keys);
                output(array(
                    "success" => "true",
                    "id" => $id
                ));
            }
            output(array(
                "success" => "true",
                "id" => $id
            ));
        } else {
            output(array(
                "success" => "false",
                "message" => "Unable to create Charter"
            ));
        }
    }

    if ($data['request'] == "charter-fetch") {
        $db->join("leagues l","c.league_id=l.id", "LEFT");
        $db->where("c.user_id", $_SESSION['id']);
        $db->where("c.active", "1");
        $db->orderBy("c.submitted != '0000-00-00', c.submitted", "desc");
        $charters = $db->get("charters c", null, "c.id, l.name as 'league', c.created, IF(c.submitted='0000-00-00','',c.submitted) as submitted");
        output($charters);
    }

    if ($data['request'] == "charter-fetch-single") {
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $db->where("id", $data['id']);
        $charters = $db->get("charters", null, "id, league_id,submitted");
        output($charters);
    }

    if ($data['request'] == "charter-delete") {
        $db->where("id", $data['id']);
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $db->where("submitted", "0000-00-00");
        $insert['active'] = 0;
        $charters = $db->update("charters", $insert);
        output($charters);
    }
    
    if ($data['request'] == "charter-submit") {
        $db->where("id", $data['id']);
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $db->where("submitted", "0000-00-00");
        $insert['submitted'] = date("Y-m-d");
        $charters = $db->update("charters", $insert);
        output($charters);
    }

    if ($data['request'] == "charter-edit") {
        //Check the charter isn't submitted
        $db->where("id", $data['id']);
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $charter = $db->get("charters", null, "submitted, league_id");
        if ($charter[0]['submitted'] !== "0000-00-00") {
            output(array(
                "success" => "false",
                "error" => "Cannot modify a submitted charter"
            ));
        }
        $league_id = $charter[0]['league_id'];

        //Validate data
        $numberArray = [];
        foreach($data['insert'] as $x) {
            $number = trim($x['number']);
            $name = trim($x['name']);
            $numberArray[] = $number;
            if ($number == "" || !ctype_digit($number)) {
                output(array(
                    "success" => "false",
                    "error" => "Invalid skater number",
                    "data" => $x
                ));
            }
            if ($name == "" || strlen($name) > 50) {
                output(array(
                    "success" => "false",
                    "error" => "Invalid skater name",
                    "data" => $x
                ));
            }
        }

        //Check if any skater number already exists in this charter
        $db->where("user_id", $_SESSION['id']);
        $db->where("charter_id", $data['id']);
        $db->where("active", "1");
        $charters = $db->get("skaters");
        foreach($charters as $row) {
            $numberArray[] = $row['number'];
        }
        if(count(array_unique($numberArray)) != count($numberArray)) {
            output(array(
                "success" => "false",
                "error" => "Duplicate roster numbers in the charter"
            ));
        }

        //Check if total number of skaters doesn't exceed 20
        if (count($data['insert']) + count($charters) > 20) {
            output(array(
                "success" => "false",
                "error" => "Too many skaters"
            ));
        }

        //Check if any skater already exists globally
        $numberArray = [];
        $nameArray = [];
        $charters = [];
        $activeDate = date("Y-m-d", strtotime('-30 days'));
        foreach($data['insert'] as $y) {
            $numberArray[] = $y['number'];
            $nameArray[] = $y['name'];
        }        
        $db->where('number', $numberArray, 'IN');
        $db->where('name', $nameArray, 'IN');
        $db->where('user_id', $_SESSION['id'], "!=");
        $db->where('active', '1');
        $skaters = $db->get("skaters", null, "number, name, charter_id, league_id");
        if ($db->count > 0) {
            //Get all unsubmitted charters and most recent submitted charter for this league
            foreach($skaters as $skater) {
                $db->where("league_id", $skater['league_id']);
                $db->where('active', '1');
                $db->where('submitted', '0000-00-00');
                $charters1 = $db->get("charters");
                $db->where("league_id", $skater['league_id']);
                $db->where('active', '1');
                $db->where('submitted != "0000-00-00"');
                $db->orderBy('submitted', 'desc');
                $charters2 = $db->get("charters", 1);
                $charters = array_merge($charters, $charters1, $charters2);
            }
            //Loop those charters and see if they have the skater in them
            foreach($charters as $charter) {
                $db->where('number', $numberArray, 'IN');
                $db->where('name', $nameArray, 'IN');
                $db->where("charter_id", $charter['id']);
                $db->where('active', '1');
                if ($db->has("skaters")) {
                    output(array(
                        "success" => "false",
                        "error" => "Skater already exists in another team's charter",
                        "data" => $skaters
                    ));
                }
            }
        }       

        //Do the insert
        $keys = Array("number","name","user_id","charter_id","active","league_id");
        $insert = [];
        foreach($data['insert'] as $z) {
            $z['user_id'] = $_SESSION['id'];
            $z['charter_id'] = $data['id'];
            $z['active'] = "1";
            $z['league_id'] = $league_id;
            $insert[] = $z;
        }
        $id = $db->insertMulti('skaters', $insert, $keys);
        if ($id) {
            output(array("success" => "true"));
        } else {
            output(array("success" => "false"));
        }
    }

    if ($data['request'] == "charter-fetch-skaters") {
        $db->join("skaters s","s.charter_id=c.id", "LEFT");
        $db->where("s.user_id", $_SESSION['id']);
        $db->where("s.charter_id", $data['id']);
        $db->where("c.active", "1");
        $db->where("s.active", "1");
        $db->orderBy("s.number", "asc");
        $charters = $db->get("charters c", null, "s.id, s.number, s.name");
        output($charters);
    }


}
?>