<?php
if (!empty($data)) {

    if ($data['request'] == "login") {
        $db->where("email", $data['email']);
        $db->where("active", "1");
        $users = $db->get("users");
        if ($db->count > 0) {
            foreach ($users as $user) {
                if (password_verify($data['password'], $user['password'])) {
                    $_SESSION['id'] = $user['id'];
                    $_SESSION['email'] = $user['email'];
                    $_SESSION['password'] = $user['password'];
                    $_SESSION['admin'] = $user['admin'];
                    output(array(
                        "id" => $user['id'],
                        "email" => $user['email'],
                        "token" => session_id()
                    ));
                }
            }

        };
        output(array("error" => "Invalid username or password"));
    }

    if ($data['request'] == "validate") {
        if (isset($_SESSION['email'])) {
            if ($_SESSION['email'] == $data['email']) {
                $db->where("email", $data['email']);
                $db->where("password", $_SESSION['password']);
                $db->where("active", "1");
                $users = $db->get("users",null,"id,admin");
                if ($db->count > 0) {
                    $user = $users[0];
                    $user['navigation'] = $user['admin'] == "1" ? "admin" : "dashboard";
                    output(array(
                        "valid" => "true",
                        "user" => $user
                    ));
                }
                output(array("valid" => "false"));
            }
            output(array("valid" => "false"));
        }
        output(array("valid" => "false"));
    }

    if ($data['request'] == "register") {
        $emailkey = substr(str_replace("/", "", base64_encode(openssl_random_pseudo_bytes(30))),0,15);
        $insert = $data['insert'];
        $insert['active'] = "0";
        $insert['password'] = password_hash($insert['password'], PASSWORD_DEFAULT);
        $insert['emailkey'] = $emailkey;
        $id = $db->insert("users",$insert);
        if ($id) {
            $data = array(
                '${url}' => $SITE_URL . "?login/activate#" . $emailkey
            );
            sendMail($insert['email'], "Activate your account", "activate", $data);
            output(array("success" => "true"));
        } else {
            output(array("success" => "false"));
        }
    }

    if ($data['request'] == "usernameavailable") {
        $db->where("email", $data['email']);
        $users = $db->get("users");
        if ($db->count == 0) {
            output(array("available" => "true"));
        }
        output(array("available" => "false"));
    }

    if ($data['request'] == "forgot") {
        $db->where("email", $data['email']);
        $db->where("active", "1");
        $users = $db->get("users");
        if ($db->count == 1) {
            $emailkey = substr(str_replace("/", "", base64_encode(openssl_random_pseudo_bytes(30))),0,15);
            $db->where("id", $users[0]['id']);
            $insert['emailkey'] = $emailkey;
            $db->update("users", $insert);
            $data = array(
                '${url}' => $SITE_URL . "?login/reset#" . $emailkey
            );
            sendMail($users[0]['email'], "Reset your password", "forgot", $data);
            output(array("message" => "Check your email account for further instructions"));
        };
        output(array("error" => "Unable to find an account with that email address"));
    }

    if ($data['request'] == "reset") {
        $insert = $data['insert'];
        $insert['emailkey'] = "";
        $insert['password'] = password_hash($insert['password'], PASSWORD_DEFAULT);
        $db->where("emailkey", $data['emailkey']);
        $id = $db->update("users",$insert);
        if ($id) {
            output(array("success" => "true"));
        } else {
            output(array("success" => "false", "error" => "Unable to reset your password"));
        }
    }

    if ($data['request'] == "activate") {
        $insert['active'] = "1";
        $insert['emailkey'] = "";
        $db->where("emailkey", $data['emailkey']);
        $id = $db->update("users",$insert);
        if ($id) {
            output(array("success" => "true"));
        } else {
            output(array("success" => "false", "error" => "Unable to activate your account"));
        }
    }
}
?>