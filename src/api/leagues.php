<?php
if (!empty($data)) {

    if ($data['request'] == "league-create") {
        if ($data['insert']['name'] =="") {
            output(array(
                "success" => "false",
                "message" => "League name cannot be empty"
            ));
        }
        $db->where("active", "1");
        $db->where("name", $data['insert']['name']);
        $leagues = $db->get("leagues", null, "id, name");
        if ($db->count > 0) {
            output(array(
                "success" => "false",
                "message" => "League already exists"
            ));
        } else {
            $insert = $data['insert'];
            $insert['user_id'] = $_SESSION['id'];
            $insert['active'] = 1;
            $id = $db->insert("leagues", $insert);
            if ($id) {
                output(array("success" => "true"));
            } else {
                output(array(
                    "success" => "false",
                    "message" => "Unable to create League"
                ));
            }
        }        
    }

    if ($data['request'] == "league-fetch") {
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $leagues = $db->get("leagues", null, "id, name");
        output($leagues);
    }

    if ($data['request'] == "league-fetch-single") {
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $db->where("id", $data['id']);
        $leagues = $db->get("leagues", null, "id, name");
        output($leagues);
    }

    if ($data['request'] == "league-delete") {
        $db->where("id", $data['id']);
        $db->where("user_id", $_SESSION['id']);
        $db->where("active", "1");
        $insert['active'] = 0;
        $leagues = $db->update("leagues", $insert);
        output($leagues);
    }

    if ($data['request'] == "league-edit") {
        $db->where("id", $data['id']);
        $db->where("user_id", $_SESSION['id']);
        $insert = $data['insert'];
        $id = $db->update("leagues", $insert);
        if ($id) {
            output(array("success" => "true"));
        } else {
            output(array("success" => "false"));
        }
    }


}
?>