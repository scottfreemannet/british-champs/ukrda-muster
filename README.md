# UKRDA Muster

This is Muster for UKRDA.

## Installation
- Clone the repo
- `npm install`
- Rename `./src/api/settings.template.php` to `settings.php` and modify
- Modify `src/scripts/index.js` to correct API URLs

## MySQL Setup
- Set up a MySQL database
- Import `db-schema.sql`
- Configure `settings.php` to your database settings

## Running in development
- `npm start`
- In a new terminal/command window, change directory to `./src/api`
- `php -S localhost:8000`

## Build for production
- `npm run build`
- Contents are in `build` folder.