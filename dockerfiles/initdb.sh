#!/usr/bin/env /bin/sh

DB_PASS=wheeliesmack
DB_USER=root
DB_NAME=muster

CONTAINER=muster_db_1

# Obviously a temporary container running an import command mounting the SQL file as a volume would be best
# ...but Docker on Windows still doesn't support Volumes very well/at all.

docker cp ../db-schema.sql $CONTAINER:/tmp/db-schema.sql
docker exec $CONTAINER mysql -hlocalhost -u$DB_USER -p$DB_PASS -e "create database $DB_NAME;"
docker exec $CONTAINER sh -c "mysql -hlocalhost -u$DB_USER -p$DB_PASS -B $DB_NAME < /tmp/db-schema.sql"
