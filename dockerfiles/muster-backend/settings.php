<?php
//General
$SITE_URL = getenv("MUSTER_SITE_URL");             //website url

//Database
$DATABASE_HOST = getenv("MUSTER_DATABASE_HOST");        //mysql server
$DATABASE_USERNAME = getenv("MUSTER_DATABASE_USERNAME");    //mysql username
$DATABASE_PASSWORD = getenv("MUSTER_DATABASE_PASSWORD");    //mysql password
$DATABASE_NAME = getenv("MUSTER_DATABASE_NAME");        //mysql database

//Email
$EMAIL_FROM = getenv("MUSTER_EMAIL_FROM");           //email from address
$SMTP_SERVER = getenv("MUSTER_SMTP_SERVER");
$SMTP_USERNAME = getenv("MUSTER_SMTP_USERNAME");
$SMTP_PASSWORD = getenv("MUSTER_SMTP_PASSWORD");
$SMTP_PORT = intval(getenv("MUSTER_SMTP_PORT", "25"));
$SMTP_USE_TLS = getenv("MUSTER_SMTP_USE_TLS", "false") == "true" ? true : false;
?>
