@echo off
setlocal

set DB_PASS=wheeliesmack
set DB_USER=root
set DB_NAME=muster

set CONTAINER=muster_db_1

REM Obviously a temporary container running an import command mounting the SQL file as a volume would be best
REM ...but Docker on Windows still doesn't support Volumes very well/at all.

docker cp ../db-schema.sql %CONTAINER%:/tmp/db-schema.sql
docker exec %CONTAINER% mysql -hlocalhost -u%DB_USER% -p%DB_PASS% -e "create database %DB_NAME%;"
REM must use sh here to enable the redirection:
docker exec %CONTAINER% sh -c "mysql -hlocalhost -u%DB_USER% -p%DB_PASS% -B %DB_NAME% < /tmp/db-schema.sql"
