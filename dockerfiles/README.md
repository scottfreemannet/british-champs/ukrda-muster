# UKRDA Muster - Containerisation
To host the entire stack locally:

- `cd dockerfiles\muster`
- `docker-compose up -d`

To bootstrap the database:
- `initdb.cmd`

This will start an instance of MariaDB, the Muster API backend as well as build and serve the front-end,
with some reasonable defaults.

The front-end will be available, by default, at http://localhost:8009 and the backend at http://localhost:8008.

During front-end development it's probably overkill to build and serve the front-end in this way; you can comment out
the `frontend` section of `docker-compose.yml` or simply change your local development front-end code to use the backend
at `http://localhost:8008`. Note the database is not exposed.

If any part of the code changes it will be necessary to run `docker-compose build` followed by commands like
`docker-compose up -d backend` to restart.
Use `docker-compose down` to cease execution and `docker-compose logs [-f]` to examine/monitor log output.

The `Dockerfiles` included are intended to support containerised deployment of the backend if necessary. Note that a
drop-in replacement `settings.php` is copied to the source tree to permit settings to be provided from environment 
variables i.a.w. best practices for containerisation.
